def step_cuadratico(pop, t, sistema):
    r = sistema.alpha
    K = -r/sistema.beta
    crecimiento_neto = r * pop[t] * (1 - pop[t]/K)
    return pop[t] + crecimiento_neto