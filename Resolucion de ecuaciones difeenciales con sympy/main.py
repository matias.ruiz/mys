import sympy as sp

# Se define el LHS de la ecuacion diferencial
t = sp.symbols("t")
x = sp.Function("x")
dxdt = sp.diff(x(t), t)
print(dxdt)

# Se definen las constantes de la ecuacion
alpha = sp.symbols(r"\alpha")
beta = sp.symbols(r"\beta")

# Se define el RHS de la ecuacion diferencial
differential_equation = sp.Eq(dxdt, alpha * x(t) + beta * x(t) ** 2)
print(differential_equation)

# Se resuelve la ecuacion diferencial
general_solution = sp.dsolve(differential_equation)
print(general_solution)

# Nos quedamos con la parte derecha de la ecuacion
rhs = general_solution.rhs
print(rhs)

# Se reemplaza la variable t por 0 para obtener el momento inicial
initial_moment = rhs.subs(t, 0)
print(initial_moment)

# Se definen C1 y x_0
C1, x_0 = sp.symbols("C1 x_0")

# Generamos la ecuacion de x_0
initial_moment_eq = sp.Eq(initial_moment, x_0)
print(initial_moment_eq)

# Se despeja C1
solutionC1 = sp.solve(initial_moment_eq, C1)
C1_0 = solutionC1[0]
print(C1_0)

# Se reemplaza C1 en la ecuacion
final_equation = rhs.subs(C1, C1_0)
final_equation_simplified = sp.simplify(final_equation)
print(final_equation_simplified) # Ecuacion final

# Al sustituir el tiempo inicial el resultado da la poblacion inicial
print(final_equation_simplified.subs(t, 0))

# Aqui se reemplazan los valores de t para obtener la prediccion del modelo poblacional
values = [(alpha, 0.025), (beta, -0.0018), (x_0, 2.56), (t, 0)]
print(final_equation_simplified.subs(values))

