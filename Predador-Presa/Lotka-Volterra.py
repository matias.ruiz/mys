import matplotlib.pyplot as plt

# Condiciones Iniciales
liebres = 500
zorros = 10

# Parametros
tasa_natalidad_liebres = 0.08
tasa_mortalidad_zorros = 0.2
capacidad_terreno = 1400
delta_time = 1
semanas = 600

probabilidad_liebre = zorros/(liebres*zorros)
probabilidad_zorro = (zorros*tasa_mortalidad_zorros)/(liebres*zorros)

lista_liebres = list()
lista_zorros = list()

lista_liebres.append(liebres)
lista_zorros.append(zorros)

temp_liebres = 0
temp_zorros = 0


for i in range(semanas):
    
    capacidad_actual = capacidad_terreno - liebres

    # Para recalcular la probabilidad en cada iteracion
    #probabilidad_liebre = zorros / (liebres * zorros)
    #probabilidad_zorro = (zorros * tasa_mortalidad_zorros) / (liebres * zorros)

    incremento_liebres = (capacidad_actual/capacidad_terreno)*tasa_natalidad_liebres*liebres
    disminucion_zorros = tasa_mortalidad_zorros*zorros

    caza = zorros*liebres

    liebres = liebres + delta_time * (incremento_liebres - (probabilidad_liebre*caza))
    zorros = zorros + delta_time * ((probabilidad_zorro * caza) - disminucion_zorros)

    lista_liebres.append(liebres)
    lista_zorros.append(zorros)

    print("Cantidad de liebres:", liebres)
    print("Cantidad de zorros:", zorros)
    print("Cantidad de semanas:", len(lista_liebres))
    if liebres <= 0 and zorros <= 0:
        break


# Graficar resultados
x = range(0, len(lista_liebres))
plt.plot(x, lista_liebres)
plt.plot(x, lista_zorros)
plt.xlabel("Semanas")
plt.ylabel("Zorros y Liebres")
plt.show()
