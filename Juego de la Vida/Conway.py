import time
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from tkinter import Tk, Label, StringVar, Button, Entry


# Variables del sistema
colors = 'white black'.split()
cmap = matplotlib.colors.ListedColormap(colors, name='colors', N=None)
analize = [[-1, -1], [-1, 0], [-1, 1], [0, 1], [1, 1], [1, 0], [1, -1], [0, -1]] # Analizador de celdas vecinas
window = Tk()
window.title("Matrix")
window.geometry("650x500+120+120") # Tamaño de la ventana
window.configure(bg='skyblue4')
window.resizable(False, False)
text_var = []
entries = []
grid_size = int(input("Insert grid size: "))
grid = []


def get_mat():
    for i in range(rows):
        grid.append([])
        for j in range(cols):
            grid[i].append(int(text_var[i][j].get()))
    window.destroy()
    print(grid)

# Estructura para introducir los datos desde el usuario
Label(window, text="Enter matrix:", font=('arial', 10, 'bold'), fg='white', bg="skyblue4").place(x=20, y=20)
x2, y2 = 0, 0
rows, cols = (grid_size, grid_size)
for i in range(rows):
    text_var.append([])
    entries.append([])
    for j in range(cols):
        text_var[i].append(StringVar())
        entry = Entry(window, textvariable=text_var[i][j], width=3)
        entry.insert(0, "0")
        entries[i].append(entry)
        entries[i][j].place(x=60 + x2, y=50 + y2)
        x2 += 30

    y2 += 30
    x2 = 0
button = Button(window, text="Load", bg='skyblue2', width=15, command=get_mat)
button.place(x=360, y=340)

window.mainloop()


temp_grid = np.zeros((grid_size, grid_size), dtype=int)
# Recorrer grilla
while True:
    for i in range(grid_size):
        for j in range(grid_size):
            temp_count = 0
            for an in analize:
                row = i+an[0]
                column = j+an[1]
                if row >= grid_size: # Corregimos el error de indice en filas
                    row = 0
                if column >= grid_size: # Corregimos el error de indice en columnas
                    column = 0
                if grid[row][column] == 1:
                    temp_count += 1
            if grid[i][j] == 0 and temp_count == 3:
                temp_grid[i][j] = 1 # Celda muerta revive
            elif grid[i][j] == 1 and (temp_count != 2 and temp_count != 3):
                temp_grid[i][j] = 0 # Celda viva muere
            else:
                temp_grid[i][j] = grid[i][j]

    # Comprobar si las matrices son iguales
    equal = True
    for i in range(grid_size):
        for j in range(grid_size):
            if grid[i][j] != temp_grid[i][j]:
                equal = False
                break
            else:
                equal = True
        if not equal:
            break
    if equal:
        time.sleep(100)

    # Actualizar matriz temporal
    for i in range(grid_size):
        for j in range(grid_size):
            grid[i][j] = temp_grid[i][j]

    # Graficar resultado
    plt.imshow(grid, cmap=cmap)
    plt.ion()
    plt.show()
    plt.pause(0.0001)
    plt.clf()

    # Tiempo muerto
    time.sleep(0.1)
