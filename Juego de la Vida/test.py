from tkinter import Tk, Label, StringVar, Button, Entry
window = Tk()
window.title("Matrix")
window.geometry("650x500+120+120")
window.configure(bg='bisque2')
window.resizable(False, False)

text_var = []
entries = []

def get_mat():
    matrix = []
    for i in range(rows):
        matrix.append([])
        for j in range(cols):
            matrix[i].append(int(text_var[i][j].get()))
    window.destroy()
    print(matrix)

Label(window, text="Enter matrix :", font=('arial', 10, 'bold'), bg="bisque2").place(x=20, y=20)

x2 = 0
y2 = 0
rows, cols = (10, 10)
for i in range(rows):
    # append an empty list to your two arrays
    # so you can append to those later
    text_var.append([])
    entries.append([])
    for j in range(cols):
        # append your StringVar and Entry
        text_var[i].append(StringVar())
        entry = Entry(window, textvariable=text_var[i][j], width=3)
        entry.insert(0, "0")
        entries[i].append(entry)
        entries[i][j].place(x=60 + x2, y=50 + y2)
        x2 += 30

    y2 += 30
    x2 = 0
button = Button(window, text="Load", bg='bisque3', width=15, command=get_mat)
button.place(x=360, y=340)

window.mainloop()

